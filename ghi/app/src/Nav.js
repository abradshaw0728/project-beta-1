import React from 'react';
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink
                exact
                to="/"
                className="nav-link"
                activeClassName="active"
              >
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to="/manufacturers"
                className="nav-link"
                activeClassName="active"
              >
                Manufacturers
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to="/create-manufacturer"
                className="nav-link"
                activeClassName="active"
              >
                Create Manufacturer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to="/vehicle-models"
                className="nav-link"
                activeClassName="active"
              >
                Vehicle Models
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to="/create-vehicle-model"
                className="nav-link"
                activeClassName="active"
              >
                Create Vehicle Model
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to="/automobiles"
                className="nav-link"
                activeClassName="active"
              >
                Automobiles
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to="/create-automobile"
                className="nav-link"
                activeClassName="active"
            >
                Create Automobile
              <NavLink className="nav-link" to="/technicians">
                Technicians
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/technicians/create">
                Add a Technician
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/create">
                Create a Service Appointment
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/history">
                Service History
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments">
                Service Appointments
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
