import React, { useState } from "react";

function VehicleModelForm() {
  const [vehicleModelData, setVehicleModelData] = useState({
    name: "",
    picture_url: "",
    manufacturer_id: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setVehicleModelData({
      ...vehicleModelData,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    fetch("/api/vehicle_models/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(vehicleModelData),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Vehicle model created:", data);
        setVehicleModelData({
          name: "",
          picture_url: "",
          manufacturer_id: "",
        });
      })
      .catch((error) => {
        console.error("Error creating vehicle model:", error);
      });
  };

  return (
    <div>
      <h2>Create a New Vehicle Model</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            id="name"
            name="name"
            value={vehicleModelData.name}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <label htmlFor="picture_url">Picture URL:</label>
          <input
            type="text"
            id="picture_url"
            name="picture_url"
            value={vehicleModelData.picture_url}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <button type="submit">Create Vehicle Model</button>
        </div>
      </form>
    </div>
  );
}

export default VehicleModelForm;
