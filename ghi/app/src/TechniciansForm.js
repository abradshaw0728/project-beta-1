import { useState } from 'react';

function TechniciansForm() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeID, setEmployeeID] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();

    const technicianData = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeID,
    };

    try {
      const response = await fetch('http://localhost:8080/api/technicians/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(technicianData),
      });

      if (response.ok) {
        const data = await response.json();
        console.log('Technician created successfully!', data);
        setFirstName('');
        setLastName('');
        setEmployeeID('');
      } else {
        console.error('Failed to create technician.');
      }
    } catch (error) {
      console.error('An error occurred:', error);
    }
  };

  return (
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">Add a Technician</h5>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="firstName" className="form-label">First Name</label>
            <input
              type="text"
              id="firstName"
              className="form-control"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="lastName" className="form-label">Last Name</label>
            <input
              type="text"
              id="lastName"
              className="form-control"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="employeeID" className="form-label">Employee ID</label>
            <input
              type="text"
              id="employeeID"
              className="form-control"
              value={employeeID}
              onChange={(e) => setEmployeeID(e.target.value)}
              required
            />
          </div>
          <button type="submit" className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  );
}

export default TechniciansForm;
