import React, { useState, useEffect } from "react";

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    fetch("/api/manufacturers/")
      .then((response) => response.json())
      .then((data) => setManufacturers(data.manufacturers))
      .catch((error) => console.error("Error fetching manufacturers", error));
  }, []);

  return (
    <div>
      <h1>Manufacturer List</h1>
      <table>
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((manufacturer) => (
            <tr key={manufacturer.id}>
              <td>{manufacturer.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ManufacturerList;
