from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
import json
from .models import AutomobileVO, Technician, Appointment
from common.json import ModelEncoder


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["date_time", "reason", "status", "vin", "customer", "technician"]
    encoders = {
        "technician": TechnicianListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": list(technicians.values())},
            encoder=TechnicianListEncoder,
            content_type="application/json",
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(
                first_name=content["first_name"],
                last_name=content["last_name"],
                employee_id=content["employee_id"],
            )
        except KeyError:
            return JsonResponse(
                {"error": "Missing required fields in the request data."},
                status=400,
                content_type="application/json",
            )
        return JsonResponse(
            {"id": technician.id},
            status=200,
            content_type="application/json",
        )


@require_http_methods(["DELETE"])
def api_technician_delete(request, id):
    technician = get_object_or_404(Technician, id=id)
    if request.method == "DELETE":
        technician.delete()
        return JsonResponse(
            {"message": "Technician deleted successfully"},
            status=200,
            content_type="application/json",
        )
    else:
        return JsonResponse(
            {"error": "Method not allowed"},
            status=400,
            content_type="application/json",
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        appointment_list = []

        for appointment in appointments:
            appointment_data = {
                "date_time": appointment.date_time,
                "reason": appointment.reason,
                "status": appointment.status,
                "vin": appointment.vin,
                "customer": appointment.customer,
                "technician": {
                    "first_name": appointment.technician.first_name,
                    "last_name": appointment.technician.last_name,
                    "employee_id": appointment.technician.employee_id,
                },
            }
            appointment_list.append(appointment_data)

        return JsonResponse(
            {"appointments": appointment_list},
            encoder=AppointmentListEncoder,
            content_type="application/json",
        )

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            appointment = Appointment.objects.create(
                date_time=content["date_time"],
                reason=content["reason"],
                vin=content["vin"],
                customer=content["customer"],
                technician=technician,
            )
        except KeyError:
            return JsonResponse(
                {"error": "Missing required fields in the request data."},
                status=400,
                content_type="application/json",
            )
        return JsonResponse(
            {"id": appointment.id},
            status=200,
            content_type="application/json",
        )


@require_http_methods(["DELETE"])
def api_appointment_delete(request, id):
    appointment = get_object_or_404(Appointment, id=id)
    if request.method == "DELETE":
        appointment.delete()
        return JsonResponse(
            {"message": "Appointment deleted successfully"},
            status=200,
            content_type="application/json",
        )
    else:
        return JsonResponse(
            {"error": "Method not allowed"},
            status=400,
            content_type="application/json",
        )


@require_http_methods(["PUT"])
def api_appointment_cancel(request, id):
    appointment = get_object_or_404(Appointment, id=id)
    if request.method == "PUT":
        appointment.status = "canceled"
        appointment.save()
        return JsonResponse(
            {"message": "Appointment status set to 'canceled'", "status": "canceled"},
            status=200,
            content_type="application/json",
        )
    else:
        return JsonResponse(
            {"error": "Method not allowed"},
            status=400,
            content_type="application/json",
        )


@require_http_methods(["PUT"])
def api_appointment_finish(request, id):
    appointment = get_object_or_404(Appointment, id=id)
    if request.method == "PUT":
        appointment.status = "finished"
        appointment.save()
        return JsonResponse(
            {"message": "Appointment status set to 'finished'", "status": "finished"},
            status=200,
            content_type="application/json",
        )
    else:
        return JsonResponse(
            {"error": "Method not allowed"},
            status=400,
            content_type="application/json",
        )
