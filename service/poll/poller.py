import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest.models import AutomobileVO


def get_updated_vins():
    response = requests.get("http://localhost:8100/api/automobiles")
    if response.status_code == 200:
        updated_vins = response.json()
        return updated_vins
    else:
        print(f"Failed to fetch VINs. Status code: {response.status_code}")
        return []


def poll(repeat=True):
    while True:
        print('Service poller polling for data')
        try:
            updated_vins = get_updated_vins()
            for vin in updated_vins:
                try:
                    automobile = AutomobileVO.objects.get(vin=vin)
                    print(f"Updated AutomobileVO with VIN: {vin}")
                except AutomobileVO.DoesNotExist:
                    print(f"VIN: {vin} does not exist in the database.")

        except Exception as e:
            print(e, file=sys.stderr)

        if (not repeat):
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
